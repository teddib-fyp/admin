class CreateNodeEventAssociation < ActiveRecord::Migration[5.1]
  def change
    create_table :node_event_associations do |t|
      t.references :node, type: :integer
      t.references :event, type: :integer
      t.timestamps
    end
  end
end
