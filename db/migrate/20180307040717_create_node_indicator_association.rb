class CreateNodeIndicatorAssociation < ActiveRecord::Migration[5.1]
  def change
    create_table :node_indicator_associations do |t|
      t.references :node, type: :integer
      t.references :indicator, type: :integer
      t.timestamps
    end
  end
end
