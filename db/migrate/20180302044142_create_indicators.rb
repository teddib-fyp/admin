class CreateIndicators < ActiveRecord::Migration[5.1]
  def change
    create_table :indicators do |t|
      t.string :name
      t.boolean :transient, default: 0
      t.float :start_value
      t.float :end_value
      t.string :nodes, default: '0'
      t.integer :ttl

      t.timestamps
    end
  end
end
