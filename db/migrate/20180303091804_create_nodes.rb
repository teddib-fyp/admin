class CreateNodes < ActiveRecord::Migration[5.1]
  def change
    create_table :nodes do |t|
      t.string :name
      t.string :state, default: '0'
      t.string :mac

      t.timestamps
    end
  end
end
