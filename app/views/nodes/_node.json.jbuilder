json.extract! node, :id, :name, :state, :mac, :created_at, :updated_at
json.url node_url(node, format: :json)
