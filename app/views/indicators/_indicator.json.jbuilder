json.extract! indicator, :id, :name, :created_at, :updated_at
json.url indicator_url(indicator, format: :json)
