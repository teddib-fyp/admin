json.extract! event, :id, :name, :dependency, :description, :created_at, :updated_at
json.url event_url(event, format: :json)
