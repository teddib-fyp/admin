class GeneratorController < ApplicationController
  before_action :authenticate_user!, except: [:sensor]
  before_action :set_generator

  def home
    set_generator_2
  end

  def create
    puts params 

    @generator = Event.new

    event_id = params[:event_id]
    indicator_id = params[:indicator_id]

    @generator.errors.add(:event_id, "Cannot be empty") if event_id.blank?
    @generator.errors.add(:indicator_id, "Cannot be empty") if indicator_id.blank?

    event = Event.find_by_id(event_id)
    indicator = Indicator.find_by_id(indicator_id)

    @generator.errors.add(:event_id, "Invalid Data") if event.blank?
    @generator.errors.add(:indicator_id, "Invalid Data") if indicator.blank?

    redirect_to root_path(@generator) and return if @generator.errors.any?

    value =  params.has_key?(:regional) ? '2' : '1'

    indicator_state_string = '0' * (indicator.id - 1) + value
    event.dependency = xor_string event.dependency, indicator_state_string
    event.save

    respond_to do |format|
      set_generator_2
      format.html { render :home }
      format.json { head :no_content }
    end
  end

  def sensor
    sensor = params[:sensor]
    mac = params[:mac]
    
    indicator = Indicator.find_by_id(sensor)
    node = Node.find_by_mac(mac)

    return if indicator.blank? or node.blank?

    indicator_state_string = '0' * (indicator.id - 1) + '1'
    node.state = xor_string node.state, indicator_state_string
    node.save

    state_check = node.state

    nodes_string = '0' * (node.id - 1) + '1'
    indicator.nodes = xor_string indicator.nodes, nodes_string
    indicator.save

    if Node.count > 2 and indicator.nodes.count('1') >= (Node.count / 2.0).ceil
      state_check[indicator.id] = '2'
    end

    NodeIndicatorAssociation.create(node_id: node.id, indicator_id: indicator.id)

    `curl -s -X POST --header 'Content-Type: application/json' \
    --header 'Accept: application/json' 'http://localhost:3000/api/StateChange' \
    -d '{ \
       "$class": "org.fyp.teddib.StateChange", \
       "indicatorId": "#{indicator.id}", \
       "node": "resource:org.fyp.teddib.Node##{node.mac}" \
     }'`

    event = Event.find_by_dependency(state_check)

    return if event.blank?

    ne = NodeEventAssociation.create(node_id: node.id, event_id: event.id)
    node_name = Node.find_by_id(ne.node_id).name
    event_name = Event.find_by_id(ne.event_id).name

    `echo "Node: #{node_name}\nEvent: #{event_name}\nTime: #{ne.created_at}" | sms 7200210789` 

  end

  private
  def set_generator
    @events = Event.all
    @indicators = Indicator.all
  end

  def set_generator_2
    @event_list = @events.where("dependency != '0'")
    @indicator_list = Array.new
    @event_list.each_with_index { |event, index|
      indicator_presence = event.dependency
      indicator_presence.size.times { |i| 
        if indicator_presence[i] != '0'
          @indicator_list[index] = Array.new if @indicator_list[index].blank?
          indicator_name = Indicator.find_by_id(i + 1).name
          indicator_name += ' (regional)' if indicator_presence[i] == '2'
          @indicator_list[index] << indicator_name
        end
      }
    }
    
    node_events = NodeEventAssociation.all
    @node_event_list = Array.new
    node_events.each { |ne|
      @node_event_list << [Node.find_by_id(ne.node_id), Event.find_by_id(ne.event_id), ne.created_at]
    }

    node_indicators = NodeIndicatorAssociation.all
    @node_indicator_list = Array.new
    node_indicators.each { |ni|
      @node_indicator_list << [Node.find_by_id(ni.node_id), Indicator.find_by_id(ni.indicator_id), ni.created_at]
    }
  end

  def xor_string a, b
    max_length = a.length > b.length ? a.length : b.length

    if a.length < max_length
      diff_length = max_length - a.length
      a << "0" * diff_length
    elsif b.length < max_length
      diff_length = max_length - b.length
      b << "0" * diff_length
    end

    refined_string = String.new
    max_length.times { |i|
      xored_value = a[i].to_i ^ b[i].to_i
      xored_value = b[i].to_i if xored_value == 3
      refined_string << xored_value.to_s
    }

    while refined_string.chomp!('0') do end

    refined_string = '0' if refined_string.length == 0

    return refined_string
  end
end
