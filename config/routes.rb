Rails.application.routes.draw do
  resources :nodes
  devise_for :users
  resources :events
  resources :indicators
  root 'generator#home'
  post '/', to: 'generator#create', as: 'generator_create'
  post '/sensor', to: 'generator#sensor', as: 'generator_sensor'
end
